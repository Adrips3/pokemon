import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { PokemonesService } from '../service/pokemones.service';
import { MatPaginator } from '@angular/material/paginator'; 
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-pokedex',
  templateUrl: './pokedex.component.html',
  styleUrls: ['./pokedex.component.scss']
})
export class PokedexComponent implements OnInit, AfterViewInit {
  
  public allPokemon : any = [];
  public seleccion  : any = [];

  public busquedaPokemon : any = [];
  
  displayedColumns: string[] = ['position', 'name', 'tipo', 'habilidad', 'img', 'botones'];

  public dataSource: any;

  public dataSlec  : any;

  public selPokemon: any;
  public imgPokemon: any;
  public tipPokemon: any;
  public habPokemon: any;

  public tarjetaSel : boolean = false;
  
  public quitarAlerta : boolean = false;
  public mensajeAlerta: any;
  public titulo: any = 'Pokemon';


  
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  constructor(
    private PokemonesService : PokemonesService
  ) {}
  
  ngOnInit(): void {
    this.getServicios();
  }

  dondeEstamos(num: number){
    switch(num){
      case 1:
          this.titulo = 'Pokemon';
        break;
      case 2:
          this.titulo = 'Mis pokemones';
        break;
    }
  }


  getServicios(){
    this.PokemonesService.crearProyectos()
    .subscribe( (data: any) =>{
      let position, nombrePokemones = data.results;
      for (let i = 0; i < nombrePokemones.length; i++) {
        let num = i;
        let name = nombrePokemones[i].name;
        this.saberEspecie(num, name);
      }
    });
  }

  saberEspecie(num: number, name: any){

    let especie: any = [];

    this.PokemonesService.obtenerEspecie(name)
    .subscribe( (data: any) =>{
      let prueba = data.egg_groups;

      for (let i = 0; i < prueba.length; i++) {
        let element = prueba[i].name;
        especie.push(element) ;
      }
      
    });
    this.saberHabilidad(num, name, especie);
  }

  saberHabilidad(position: number, name : any, especie: any){
    let habilidades: any = [];
    this.PokemonesService.obtenerHabilidad(name)

    .subscribe( (data: any) =>{
      let prueba = data.abilities;
      for (let i = 0; i < prueba.length; i++) {
        let element = prueba[i].ability.name;
        let img
        habilidades.push(element);
      }
    });
    let img = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${position+1}.png`;
    this.allPokemon.push({'position': position, 'name': name, 'especie': especie, 'habilidades': habilidades, 'img': img})
    this.dataSource = new MatTableDataSource(this.allPokemon);
   
  }

  detallePokemon( pokemonSeleccionado: number ){
    this.tarjetaSel = true;

    let pokedex: any;

    pokedex   = document.getElementById('tabla');

    pokedex.classList.remove("col-12");
    pokedex.classList.add("col-7");

    let selPokemon  = this.allPokemon[pokemonSeleccionado];
    this.selPokemon = selPokemon.name;
    this.imgPokemon = selPokemon.img;
    this.tipPokemon = selPokemon.especie;
    this.habPokemon = selPokemon.habilidades;
  }

  cerrar(){
    this.tarjetaSel = false;
    let pokedex: any;

    pokedex = document.getElementById('tabla');

    pokedex.classList.remove("col-7");
    pokedex.classList.add("col-12");
  }

  agregar(pokemonAgregado: number){

    if( this.seleccion.includes( this.allPokemon[pokemonAgregado] ) == false ){
      this.seleccion.push( this.allPokemon[pokemonAgregado] );
      this.dataSlec = new MatTableDataSource(this.seleccion); 
      
      this.quitarAlerta   = true; 
      this.mensajeAlerta  =  `Se agrego correctame el siguiente pokemon ${this.allPokemon[pokemonAgregado].name} `;
    }else{
      this.quitarAlerta   = true;
      this.mensajeAlerta  =  `Ya existe el siguiente pokemon ${this.allPokemon[pokemonAgregado].name}, en tu selección `;
    }
    setTimeout(()=>{this.cerrarMensaje()},5000);
  }

  cerrarMensaje(){
    this.quitarAlerta   = false;
  }

  borrar(pokemonBorrado: number) {

    this.seleccion.splice( pokemonBorrado, 1 );  
    console.log(this.seleccion)
    this.dataSlec = new MatTableDataSource(this.seleccion);
    this.quitarAlerta   = true;
    this.mensajeAlerta  =  `Pokemon borrado exitosamente `;
    setTimeout(()=>{this.cerrarMensaje()},5000)
  }

  recibirDatos(solicitud : any){
    let datoBusqueda = solicitud.value;

    if(datoBusqueda === undefined){
      return this.dataSource;
    }

    let nuevoArreglo = this.allPokemon.filter(function(item : any){
      return item.name.toLowerCase().includes(datoBusqueda.toLowerCase());
    });

    this.dataSource = new MatTableDataSource(nuevoArreglo);
  }

}
