import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PokemonesService {

  private url       = 'https://pokeapi.co/api/v2/';
  private tipo      = 'https://pokeapi.co/api/v2/';

  constructor( private http : HttpClient ) { }

  crearProyectos(){
    return this.http.get(`${ this.url }pokemon?limit=250`);
  }

  obtenerEspecie(pokemon: any){
    return this.http.get(`${ this.tipo }pokemon-species/${pokemon}`);
  }

  obtenerHabilidad(pokemon: any ){
    return this.http.get(`${ this.tipo }pokemon/${pokemon}`);
  }

}
